import { Cartesian3, Ion, ScreenSpaceEventHandler, ScreenSpaceEventType, Viewer } from "cesium";

(window as any).CESIUM_BASE_URL = ".";
Ion.defaultAccessToken =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZThhOTZlNS05MzgwLTRkNTYtODc1Yy01MTYwMzI3NWUyZmIiLCJpZCI6MjQzNzcsImlhdCI6MTY1NjU1MjkzMH0.XzOOAtPz4ZcVQiQxo5BG5gbeVWtdmTtgwa-5MjCl7-s";

let mapDom = document.createElement("div");
mapDom.style.width = "100vw";
mapDom.style.height = "100vh";
mapDom.id = "app";
document.body.appendChild(mapDom);

let viewer = new Viewer("app", {
  shouldAnimate: true,
});

let scene = viewer.scene;
let canvas = viewer.canvas;
let ellipsoid = scene.globe.ellipsoid;

scene.screenSpaceCameraController.enableRotate = false;
scene.screenSpaceCameraController.enableTranslate = false;
scene.screenSpaceCameraController.enableZoom = false;
scene.screenSpaceCameraController.enableTilt = false;
scene.screenSpaceCameraController.enableLook = false;

let flags = {
  looking: false,
  moveForward: false,
  moveBackward: false,
  moveUp: false,
  moveDown: false,
  moveLeft: false,
  moveRight: false,
};

let handler = new ScreenSpaceEventHandler(canvas);
let startPosition, movePosition;
handler.setInputAction(movement => {
	flags.looking = true;
	movePosition = startPosition = Cartesian3.clone(movement.position);
}, ScreenSpaceEventType.LEFT_DOWN)
handler.setInputAction(movement => {
	movePosition = movement.endPosition;
}, ScreenSpaceEventType.MOUSE_MOVE)
handler.setInputAction(movement => {
	flags.looking = false;
}, ScreenSpaceEventType.LEFT_UP)

document.addEventListener(
  "keydown",
  (e) => {
    switch (e.key) {
      case "a":
        flags.moveLeft = true;
        break;
      case "s":
        flags.moveDown = true;
        break;
      case "d":
        flags.moveRight = true;
        break;
      case "w":
        flags.moveUp = true;
        break;
      default:
        break;
    }
  },
  false
);
document.addEventListener(
  "keyup",
  (e) => {
    switch (e.key) {
      case "a":
        flags.moveLeft = false;
        break;
      case "s":
        flags.moveDown = false;
        break;
      case "d":
        flags.moveRight = false;
        break;
      case "w":
        flags.moveUp = false;
        break;
      default:
        break;
    }
  },
  false
);

viewer.clock.onTick.addEventListener((clock) => {
  let camera = viewer.camera;
  let cameraHeight = ellipsoid.cartesianToCartographic(camera.position).height;
  let moveRate = cameraHeight / 100.0;

  if(flags.looking){
	  let width = canvas.width;
	  let height = canvas.height;

	  let x = (movePosition.x - startPosition.x) / width;
	  let y = (movePosition.y - startPosition.y) / height;

	  let lookFactor = 0.05;
	  camera.lookRight(x * lookFactor);
	  camera.lookUp(y * lookFactor);
  }

  if (flags.moveUp) {
    camera.moveUp(moveRate);
  }
  if (flags.moveDown) {
    camera.moveDown(moveRate);
  }
  if (flags.moveLeft) {
    camera.moveLeft(moveRate);
  }
  if (flags.moveRight) {
    camera.moveRight(moveRate);
  }
});
