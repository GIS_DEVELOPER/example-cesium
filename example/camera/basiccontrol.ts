import { Math, Cartesian3, Cartographic, Ion, ScreenSpaceEventHandler, ScreenSpaceEventType, Viewer } from "cesium";
import { Menu } from "../ui/menu";

(window as any).CESIUM_BASE_URL = ".";
Ion.defaultAccessToken =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZThhOTZlNS05MzgwLTRkNTYtODc1Yy01MTYwMzI3NWUyZmIiLCJpZCI6MjQzNzcsImlhdCI6MTY1NjU1MjkzMH0.XzOOAtPz4ZcVQiQxo5BG5gbeVWtdmTtgwa-5MjCl7-s";

let mapDom = document.createElement("div");
mapDom.style.width = "100vw";
mapDom.style.height = "100vh";
mapDom.id = "app";
document.body.appendChild(mapDom);

let viewer = new Viewer("app", {
  shouldAnimate: true,
});

// let handler = new ScreenSpaceEventHandler(viewer.canvas);
// viewer.scene.canvas.addEventListener('click', e => {
//     var ellipsoid = viewer.scene.globe.ellipsoid;
//     var cartesian = viewer.camera.pickEllipsoid(new Cartesian3(e.clientX, e.clientY), ellipsoid);
//     var cartographic = ellipsoid.cartesianToCartographic(cartesian);
//     console.log(cartographic);
// });

let menu = new Menu();
menu.createElement("button", "fly1", () => {
  viewer.camera.flyTo({
    destination: Cartesian3.fromDegrees(121.49600307700018, 31.24021414268914, 10000)
  });
});

menu.createElement("button", "setView", () => {
  viewer.camera.setView({
    destination: Cartesian3.fromDegrees(121.475092824988, 31.188366955416868, 10000),
    orientation: {
      heading: Math.toRadians(30),
      pitch: Math.toRadians(-60),
      roll: Math.toRadians(-30)
    }
  });
});