import { Billboard, Cartesian3, CesiumWidget, Ion, NearFarScalar, Viewer } from "cesium";

(window as any).CESIUM_BASE_URL = '.';
Ion.defaultAccessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZThhOTZlNS05MzgwLTRkNTYtODc1Yy01MTYwMzI3NWUyZmIiLCJpZCI6MjQzNzcsImlhdCI6MTY1NjU1MjkzMH0.XzOOAtPz4ZcVQiQxo5BG5gbeVWtdmTtgwa-5MjCl7-s";

let mapDom = document.createElement("div");
mapDom.id = "app";
mapDom.style.width = "100vw";
mapDom.style.height = "100vh";
document.body.append(mapDom);

let viewer: Viewer = new Viewer('app');
viewer.scene.camera.setView({
    destination: Cartesian3.fromDegrees(121, 31, 300000)
});

viewer.entities.add({
    position: Cartesian3.fromDegrees(121, 31),
    billboard: {
        image: './Assets/Images/cesium_credit.png',
        scaleByDistance: new NearFarScalar(1.5e2, 2, 1.5e7, .5),
        translucencyByDistance: new NearFarScalar(1.5e2, 2, 1.5e7, .5)
    }
})