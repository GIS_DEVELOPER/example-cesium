const menus = [
  {
    desc: "空白地球",
    name: "earth",
  },
  {
    desc: "添加ArcGIS MapServer数据源",
    name: "arcgisserver",
  },
  {
    desc: "添加WMS数据源",
    name: "wmsserver",
  },
  {
    desc: "添加WMTS数据源",
    name: "wmtsserver",
  },
  {
    desc: "添加BillBoard",
    name: "billboard",
  },
  {
    desc: "修改图层alpha通道",
    name: "alpha",
  },
  {
    desc: "时钟动画",
    name: "clock",
  },
  {
    desc: "绘制几何体并显示/隐藏",
    name: "showGeometry",
  },
  {
    desc: "czml-绘制立方体",
    name: "czml_box",
  },
  {
    desc: "DataSource-JSON",
    name: "ds_json",
  },
  {
    desc: "3d_tiles设置样式",
    name: "3dtiles/3dtiles_styling",
  },
  {
    desc: "3d_tiles选择要素",
    name: "3dtiles/3dtiles_hover",
  },
  {
    desc: "3d_tile点云分类",
    name: "3dtiles/3dtiles_pointcloud_classification"
  },
  {
    desc: "3d_tile本地加载",
    name: "3dtiles/3dtiles_local_load"
  },
  {
    desc: "3d_tile加载bim",
    name: "3dtiles/3dtiles_load_bim"
  },
  {
    desc: "测试",
    name: "test",
  },
  {
    desc: "模型-飞机",
    name: "model/airplane",
  },
  {
    desc: "相机-基本控制",
    name: "camera/basiccontrol",
  },
  {
    desc: "相机-鼠标控制",
    name: "camera/keycontrol",
  },
  {
    desc: "贴地-简单地物",
    name: "clamp/terrainClamp",
  },
  {
    desc: "动画-飞行",
    name: "animate/car"
  }
];

function generateMenus() {
  let ul = document.createElement("ul");

  menus.forEach((menu) => {
    let li = document.createElement("li");
    let a = document.createElement("a");
    a.innerHTML = menu.desc;
    a.setAttribute("href", `#/${menu.name}`);
    li.appendChild(a);
    ul.appendChild(li);
  });
  document.body.appendChild(ul);
}

document.body.innerHTML = "";
let currentUrl = location.hash.substring(2);
if (!currentUrl) {
  generateMenus();
} else {
  require(`./${currentUrl}.ts`);
}

window.addEventListener("hashchange", () => {
  document.body.innerHTML = "";
  let url = location.hash.substring(2);
  if (url) {
    require(`./${url}.ts`);
  }
});

window.addEventListener("popstate", function () {
  if (location.hash === "") {
    location.reload();
  }
});
