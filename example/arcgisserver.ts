import {
  CesiumWidget,
  ArcGisMapServerImageryProvider,
  Rectangle,
  Ion,
} from "cesium";

(window as any).CESIUM_BASE_URL = ".";
Ion.defaultAccessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZThhOTZlNS05MzgwLTRkNTYtODc1Yy01MTYwMzI3NWUyZmIiLCJpZCI6MjQzNzcsImlhdCI6MTY1NjU1MjkzMH0.XzOOAtPz4ZcVQiQxo5BG5gbeVWtdmTtgwa-5MjCl7-s";

let mapDom = document.createElement("div");
mapDom.id = "app";
mapDom.style.width = "100vw";
mapDom.style.height = "100vh";
document.body.append(mapDom);

let widget: CesiumWidget;
widget = new CesiumWidget("app");

let imageryProvider = widget.imageryLayers;
imageryProvider.addImageryProvider(
  new ArcGisMapServerImageryProvider({
    url:
      "http://services.ga.gov.au/gis/rest/services/Liquid_Fuel_Facilities/MapServer",
  })
);

widget.camera.setView({
  destination: Rectangle.fromDegrees(114.591, -45.837, 148.97, -5.73),
});
