import { Cartesian3, CesiumWidget, Color, Ion, Viewer } from "cesium";

(window as any).CESIUM_BASE_URL = ".";
Ion.defaultAccessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZThhOTZlNS05MzgwLTRkNTYtODc1Yy01MTYwMzI3NWUyZmIiLCJpZCI6MjQzNzcsImlhdCI6MTY1NjU1MjkzMH0.XzOOAtPz4ZcVQiQxo5BG5gbeVWtdmTtgwa-5MjCl7-s";

let mapDom = document.createElement('div');
mapDom.id = "app";
mapDom.style.width = "100vw";
mapDom.style.height = "100vh";
document.body.append(mapDom);

let control = document.createElement('div');
document.body.appendChild(control);
control.style.position = 'absolute';
control.style.left = '20px';
control.style.top = '20px';
control.style.zIndex = '100';
let boxBtn = document.createElement('button');
boxBtn.innerText = 'toggle box';
boxBtn.onclick = function(){
    let entity = entities.getById('box');
    entity.show = !entity.show;
}
let circleBtn = document.createElement('button');
circleBtn.innerText = 'toggle circle';
circleBtn.onclick = function(){
    let entity = entities.getById('circle');
    entity.show = !entity.show;
}
control.appendChild(boxBtn);
control.appendChild(circleBtn);

let viewer: Viewer;
viewer = new Viewer("app");
// viewer.scene.camera.setView({
//     destination: Cartesian3.fromDegrees(121, 32, 300000)
// })

let entities = viewer.entities;
entities.add({
    id: 'box',
    position: Cartesian3.fromDegrees(121, 32, 300000),
    box: {
        dimensions: new Cartesian3(90000, 90000, 90000),
        material: new Color(1.0, 0, 0, 1)
    }
})
entities.add({
    id: 'circle',
    position: Cartesian3.fromDegrees(122, 32, 300000),
    ellipsoid: {
       radii: new Cartesian3(45000, 45000, 45000), 
       material: Color.fromRandom({alpha: 1.0})
    }
});

viewer.flyTo(entities)