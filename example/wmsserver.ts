import { CesiumWidget, Ion, Rectangle, WebMapServiceImageryProvider } from "cesium";

(window as any).CESIUM_BASE_URL = ".";
Ion.defaultAccessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZThhOTZlNS05MzgwLTRkNTYtODc1Yy01MTYwMzI3NWUyZmIiLCJpZCI6MjQzNzcsImlhdCI6MTY1NjU1MjkzMH0.XzOOAtPz4ZcVQiQxo5BG5gbeVWtdmTtgwa-5MjCl7-s";

let mapDom = document.createElement("div");
mapDom.id = "app";
mapDom.style.width = "100vw";
mapDom.style.height = "100vh";
document.body.append(mapDom);

let widget = new CesiumWidget("app");

let imageryLayers = widget.imageryLayers;
imageryLayers.addImageryProvider(
  new WebMapServiceImageryProvider({
    url: "https://geowebservices.stanford.edu/geoserver/ows",
    layers: "druid:dw886jf2441",
    parameters: { SERVICE: "WMS", transparent: true, format: "image/png" },
  })
);

widget.camera.setView({
  destination: Rectangle.fromDegrees(120.8582, 30.7787, 121.93075, 31.50829),
});
