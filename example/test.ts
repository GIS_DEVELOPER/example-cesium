import { ArcGisMapServerImageryProvider, ImagerySplitDirection, Ion, Viewer } from "cesium";

(window as any).CESIUM_BASE_URL = ".";
Ion.defaultAccessToken =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZThhOTZlNS05MzgwLTRkNTYtODc1Yy01MTYwMzI3NWUyZmIiLCJpZCI6MjQzNzcsImlhdCI6MTY1NjU1MjkzMH0.XzOOAtPz4ZcVQiQxo5BG5gbeVWtdmTtgwa-5MjCl7-s";

let mapDom = document.createElement("div");
mapDom.id = "app";
mapDom.style.width = "100vw";
mapDom.style.height = "100vh";
document.body.append(mapDom);

var viewer = new Viewer("app", {
  baseLayerPicker: false,
  infoBox: false,
});
viewer.scene.imagerySplitPosition = .5;

let imageryLayers = viewer.imageryLayers;
let layer = imageryLayers.addImageryProvider(
  new ArcGisMapServerImageryProvider({
    url: "https://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer",
  })
);
// layer.splitDirection = ImagerySplitDirection.LEFT;

