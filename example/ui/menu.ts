export class Menu{
    private container: HTMLElement;
    constructor(){
        let cacheNode = document.querySelector(".ctrl-container");
        if(cacheNode){
            document.body.removeChild(cacheNode);;
        }
        this.container = document.createElement('div');
        this.container.classList.add("ctrl-container");
        this.container.style.position = "absolute";
        this.container.style.left = "20px";
        this.container.style.top = "20px";

        document.body.appendChild(this.container);
    }

    public createElement(type: string, text: string, callback: () => void){
        let typeDom = document.createElement(type);
        typeDom.style.cursor = "pointer";
        typeDom.innerText = text;
        typeDom.addEventListener('click', callback);
        this.container.append(typeDom);
    };
}
