import { Cartesian3, Color, Ion, Rectangle, SingleTileImageryProvider, Viewer } from "cesium";

(window as any).CESIUM_BASE_URL = ".";
Ion.defaultAccessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZThhOTZlNS05MzgwLTRkNTYtODc1Yy01MTYwMzI3NWUyZmIiLCJpZCI6MjQzNzcsImlhdCI6MTY1NjU1MjkzMH0.XzOOAtPz4ZcVQiQxo5BG5gbeVWtdmTtgwa-5MjCl7-s";

let mapDom = document.createElement("div");
mapDom.id = "app";
mapDom.style.width = "100vw";
mapDom.style.height = "100vh";
document.body.append(mapDom);
let viewer: Viewer = new Viewer("app");

// add control
let control = document.createElement("input");
control.setAttribute("type", "range");
control.style.position = "absolute";
control.style.left = "20px";
control.style.top = "20px";
control.addEventListener("change", (e) => {
  let num = (e.target as any).valueAsNumber;
  console.log(num / 100);
  baseLayer.colorToAlphaThreshold = num / 100;
  // TODO
  // baseLayer.colorToAlpha = new Color(num/100, 0, 0);
});
document.body.appendChild(control);

// add imageryLayer
let layers = viewer.imageryLayers;
let baseLayer = layers.addImageryProvider(new SingleTileImageryProvider({
    url: require('../public/static/images/earthbump1k.jpg').default,
    rectangle: Rectangle.fromDegrees(-180.0, -90.0, 180.0, 90.0)
}))
baseLayer.colorToAlpha = new Color(1, 0, 0);
baseLayer.colorToAlphaThreshold = .2;