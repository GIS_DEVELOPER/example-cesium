import { Cartesian3, Color, ColorBlendMode, HeadingPitchRoll, Ion, Transforms, Viewer} from "cesium";

(window as any).CESIUM_BASE_URL = ".";
Ion.defaultAccessToken =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZThhOTZlNS05MzgwLTRkNTYtODc1Yy01MTYwMzI3NWUyZmIiLCJpZCI6MjQzNzcsImlhdCI6MTY1NjU1MjkzMH0.XzOOAtPz4ZcVQiQxo5BG5gbeVWtdmTtgwa-5MjCl7-s";


let mapDom = document.createElement("div");
mapDom.style.width = "100vw";
mapDom.style.height = "100vh";
mapDom.id = "app";
document.body.appendChild(mapDom);

let viewer = new Viewer("app", {
	shouldAnimate: true
});
let position = Cartesian3.fromDegrees(118, 32, 0);
let hpr = new HeadingPitchRoll(0, 0, 0);
let orientation = Transforms.headingPitchRollQuaternion(position, hpr);
let uri = "http://localhost:3000/static/model/GroundVehicle.glb";
let entity = viewer.entities.add({
	name: uri,
	position: position,
	// @ts-ignore
	orientation: orientation,
	model: {
		uri: uri,
		minimumPixelSize: 0,
		color: new Color(1, 0, 0, 1),
		colorBlendMode: ColorBlendMode.HIGHLIGHT,
		colorBlendAmount: .5
	}
});
viewer.trackedEntity = entity;