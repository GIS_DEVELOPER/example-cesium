import { Cartesian3, Cesium3DTileStyle, CesiumWidget, createOsmBuildings, Ion, Viewer, viewerCesium3DTilesInspectorMixin } from "cesium";

(window as any).CESIUM_BASE_URL = ".";
Ion.defaultAccessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZThhOTZlNS05MzgwLTRkNTYtODc1Yy01MTYwMzI3NWUyZmIiLCJpZCI6MjQzNzcsImlhdCI6MTY1NjU1MjkzMH0.XzOOAtPz4ZcVQiQxo5BG5gbeVWtdmTtgwa-5MjCl7-s";

let mapDom = document.createElement('div');
mapDom.id = "app";
mapDom.style.width = "100vw";
mapDom.style.height = "100vh";
document.body.append(mapDom);

let widget: CesiumWidget;
widget = new CesiumWidget("app");

let building = createOsmBuildings()
building.style = new Cesium3DTileStyle({
    defines: {
      material: "${feature['building:material']}",
    },
    color: {
      conditions: [
        ["${material} === null", "color('white')"],
        ["${material} === 'glass'", "color('skyblue', 0.5)"],
        ["${material} === 'concrete'", "color('grey')"],
        ["${material} === 'brick'", "color('indianred')"],
        ["${material} === 'stone'", "color('lightslategrey')"],
        ["${material} === 'metal'", "color('lightgrey')"],
        ["${material} === 'steel'", "color('lightsteelblue')"],
        ["true", "color('white')"], // This is the else case
      ],
    },
  });
widget.scene.primitives.add(building);

widget.scene.camera.setView({
    destination: Cartesian3.fromDegrees(121.4, 31.2, 300)
})
