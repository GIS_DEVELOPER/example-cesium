import {
  Cartesian3,
  Ion,
  Cesium3DTileset,
  HeadingPitchRoll,
  Viewer,
  HeadingPitchRange,
} from "cesium";
(window as any).CESIUM_BASE_URL = ".";

let div = document.createElement("div");
div.setAttribute("id", "map");
div.style.width = "100vw";
div.style.height = "100vh";
document.body.append(div);
Ion.defaultAccessToken =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZThhOTZlNS05MzgwLTRkNTYtODc1Yy01MTYwMzI3NWUyZmIiLCJpZCI6MjQzNzcsImlhdCI6MTY1NjU1MjkzMH0.XzOOAtPz4ZcVQiQxo5BG5gbeVWtdmTtgwa-5MjCl7-s";

let viewer = new Viewer("map");

let rawTileset = new Cesium3DTileset({
  url: "http://localhost:9003/model/t4EaYtrAb/tileset.json",
});
viewer.scene.primitives.add(rawTileset);
rawTileset.readyPromise.then((tileset) => {
  viewer.scene.primitives.add(tileset);
  viewer.zoomTo(
    tileset,
    new HeadingPitchRange(0.5, -0.2, tileset.boundingSphere.radius * 1.0)
  );
});
viewer.camera.setView({
  destination: Cartesian3.fromDegrees(120.48187, 36.07594, 1000),
  orientation: new HeadingPitchRoll(0.25, -.5)
})
