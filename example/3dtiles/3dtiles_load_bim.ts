import {
    Cartesian3,
    Ion,
    Cesium3DTileset,
    createWorldTerrain,
    HeadingPitchRoll,
    IonResource,
    Viewer,
    Matrix4,
    HeadingPitchRange,
    ScreenSpaceEventHandler,
  } from "cesium";
  (window as any).CESIUM_BASE_URL = ".";
  
  let div = document.createElement("div");
  div.setAttribute("id", "map");
  div.style.width = "100vw";
  div.style.height = "100vh";
  document.body.append(div);
  Ion.defaultAccessToken =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZThhOTZlNS05MzgwLTRkNTYtODc1Yy01MTYwMzI3NWUyZmIiLCJpZCI6MjQzNzcsImlhdCI6MTY1NjU1MjkzMH0.XzOOAtPz4ZcVQiQxo5BG5gbeVWtdmTtgwa-5MjCl7-s";
  
  let viewer = new Viewer("map");
  
  let rawTileset = new Cesium3DTileset({
    url: "http://localhost:9003/model/tzsKNeZ5Y/tileset.json"
  });
  rawTileset.readyPromise.then((tileset) => {
    viewer.scene.primitives.add(tileset);
    viewer.zoomTo(
      tileset,
      new HeadingPitchRange(0.5, -0.2, tileset.boundingSphere.radius * 1.0)
    );
  });

  // const handler = ScreenSpaceEventHandler(scene)
  