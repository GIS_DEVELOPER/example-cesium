import {
  Cartesian3,
  CesiumWidget,
  Color,
  createOsmBuildings,
  HeadingPitchRoll,
  Ion,
  ScreenSpaceEventType,
  Viewer,
} from "cesium";

(window as any).CESIUM_BASE_URL = ".";
Ion.defaultAccessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZThhOTZlNS05MzgwLTRkNTYtODc1Yy01MTYwMzI3NWUyZmIiLCJpZCI6MjQzNzcsImlhdCI6MTY1NjU1MjkzMH0.XzOOAtPz4ZcVQiQxo5BG5gbeVWtdmTtgwa-5MjCl7-s";

let mapDom = document.createElement("div");
mapDom.id = "app";
mapDom.style.width = "100vw";
mapDom.style.height = "100vh";
document.body.append(mapDom);

let widget: CesiumWidget;
widget = new CesiumWidget("app");
widget.scene.camera.setView({
  destination: Cartesian3.fromDegrees(121.4, 31.2, 3000),
  orientation: HeadingPitchRoll.fromDegrees(55, -22, 0),
});

let building = createOsmBuildings();
widget.scene.primitives.add(building);

let cache = {
  feature: null,
  originColor: null,
};
widget.screenSpaceEventHandler.setInputAction((movement) => {
  let feature = widget.scene.pick(movement.position);
  if (cache.feature && feature !== cache.feature) {
    cache.feature.color = cache.originColor;
  }
  if (feature) {
    cache.feature = feature;
    cache.originColor = Color.clone(feature.color);
    feature.color = new Color(1.0, 0, 0, 1.0);
  }
}, ScreenSpaceEventType.LEFT_CLICK);
