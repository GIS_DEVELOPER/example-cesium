import { Cartesian3, Ion, Cesium3DTileset, createWorldTerrain, HeadingPitchRoll, IonResource, Viewer } from "cesium";
(window as any).CESIUM_BASE_URL = ".";

let div = document.createElement("div");
div.setAttribute("id", "map");
div.style.width = "100vw";
div.style.height = "100vh";
document.body.append(div);
Ion.defaultAccessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZThhOTZlNS05MzgwLTRkNTYtODc1Yy01MTYwMzI3NWUyZmIiLCJpZCI6MjQzNzcsImlhdCI6MTY1NjU1MjkzMH0.XzOOAtPz4ZcVQiQxo5BG5gbeVWtdmTtgwa-5MjCl7-s";

let viewer = new Viewer("map", {
  terrainProvider: createWorldTerrain(),
});

let tileset = new Cesium3DTileset({
  url: IonResource.fromAssetId(16421),
});
viewer.scene.primitives.add(tileset);

viewer.scene.camera.setView({
  destination: new Cartesian3(
    4401744.644145314,
    225051.41078911052,
    4595420.374784433
  ),
  orientation: new HeadingPitchRoll(
    5.646733805039757,
    -0.276607153839886,
    6.281110875400085
  ),
});
