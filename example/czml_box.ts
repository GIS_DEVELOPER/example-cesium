import { CesiumWidget, CzmlDataSource, Ion, Viewer } from "cesium";

(window as any).CESIUM_BASE_URL = ".";
Ion.defaultAccessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZThhOTZlNS05MzgwLTRkNTYtODc1Yy01MTYwMzI3NWUyZmIiLCJpZCI6MjQzNzcsImlhdCI6MTY1NjU1MjkzMH0.XzOOAtPz4ZcVQiQxo5BG5gbeVWtdmTtgwa-5MjCl7-s";

let mapDom = document.createElement("div");
mapDom.id = "app";
mapDom.style.width = "100vw";
mapDom.style.height = "100vh";
document.body.append(mapDom);

let widget: Viewer;
widget = new Viewer("app");

let czml = [
  {
    id: "document",
    name: "box",
    version: "1.0",
  },
  {
    id: "shape1",
    position: {
      cartographicDegrees: [121, 30, 300000],
    },
    box: {
      dimensions: {
        cartesian: [400000, 300000, 500000],
      },
      material: {
        solidColor: {
          color: {
            rgba: [0, 0, 255, 255],
          },
        },
      },
    },
  },
];

let czmlSource = CzmlDataSource.load(czml);
widget.dataSources.add(czmlSource);
widget.zoomTo(czmlSource);
