import { CesiumWidget, Ion, TimeIntervalCollection, Viewer, WebMapTileServiceImageryProvider } from "cesium";

(window as any).CESIUM_BASE_URL = ".";
Ion.defaultAccessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ZThhOTZlNS05MzgwLTRkNTYtODc1Yy01MTYwMzI3NWUyZmIiLCJpZCI6MjQzNzcsImlhdCI6MTY1NjU1MjkzMH0.XzOOAtPz4ZcVQiQxo5BG5gbeVWtdmTtgwa-5MjCl7-s";

let mapDom = document.createElement("div");
mapDom.id = "app";
mapDom.style.width = "100vw";
mapDom.style.height = "100vh";
document.body.append(mapDom);

let viewer = new Viewer("app", { shouldAnimate: true });
let imagerLayers = viewer.imageryLayers;
imagerLayers.addImageryProvider(
  new WebMapTileServiceImageryProvider({
    url:
      "https://gibs.earthdata.nasa.gov/wmts/epsg4326/best/MODIS_Terra_CorrectedReflectance_TrueColor/default/2015-08-23T00%3A00%3A00Z/{TileMatrixSet}/{TileMatrix}/{TileRow}/{TileCol}.jpg",
    layer: "MODIS_Terra_CorrectedReflectance_TrueColor",
    style: "default",
    tileMatrixSetID: "250m",
    maximumLevel: 5,
    format: "image/jpeg",
    credit: "NASA Global Imagery Browse Services for EOSDIS",
  })
);
