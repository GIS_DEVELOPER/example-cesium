const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopywebpackPlugin = require("copy-webpack-plugin");

let cesiumSource = "node_modules/cesium/Source";
let cesiumWorkers = "../Build/Cesium/Workers";

module.exports = {
  mode: "development",
  devtool: "eval-source-map",
  devServer: {
    contentBase: path.resolve(__dirname, "./dist"),
    host: "0.0.0.0",
    port: 3000,
  },
  entry: path.resolve(__dirname, "./main.js"),
  output: {
    path: path.resolve(__dirname, "./dist"),
    filename: "[hash].js",
  },
  resolve: {
    extensions: [".ts", ".js", ".obj"],
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: [
          {
            loader: "ts-loader",
          },
        ],
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(gif|png|jpg|obj|fbx|glb|gltf|ply|hdr)$/,
        use: [
          {
            loader: "url-loader",
          },
        ],
      },
    ],
  },
  experiments:{
    topLevelAwait: true
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "./public/index.html"),
      filename: "index.html",
    }),
    new CopywebpackPlugin({
      patterns: [
        { from: path.join(cesiumSource, cesiumWorkers), to: "Workers" },
        { from: path.join(cesiumSource, "Assets"), to: "Assets" },
        { from: path.join(cesiumSource, "Widgets"), to: "Widgets" },
        { from: path.resolve(__dirname, './public/static'), to: "static"}
      ],
    }),
  ],
};
